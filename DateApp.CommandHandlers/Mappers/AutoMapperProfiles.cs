﻿using AutoMapper;
using DateApp.Domain;
using DateApp.Domain.Entities;
using DateApp.Models.Models;
using System.Linq;

namespace DateApp.CommandHandlers.Mappers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<AppUser, MemberModel>()
                .ForMember(dest => dest.PhotoUrl,opt => opt.MapFrom(src => src.Photos.FirstOrDefault(x => x.IsMain).Url))
                .ForMember(dest => dest.Age, opt => opt.MapFrom(src => src.DateOfBirth.CalculateAge()));
            CreateMap<Photo, PhotoModel>();
            CreateMap<MemberUpdateModel, AppUser>();
        }
    }
}
