﻿using DateApp.Domain.Entities;
using DateApp.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DateApp.CommandHandlers.Repository
{
    public interface IUserRepository
    {
        void Update(AppUser user);
        Task<bool> SaveAllAsync();
        Task<IEnumerable<AppUser>> GetUsersAsync();
        Task<AppUser> GetUserByIDAsync(int id);
        Task<AppUser> GetUserByUsernameAsync(string username);
        Task<IEnumerable<MemberModel>> GetMembersAsync();
        Task<MemberModel> GetMemberAsync(string username);
    }
}
