﻿using Microsoft.AspNetCore.Mvc;

namespace DateApp.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseApiController : ControllerBase
    { }
}
