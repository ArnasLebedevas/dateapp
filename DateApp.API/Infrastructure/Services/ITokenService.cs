﻿using DateApp.Domain.Entities;

namespace DateApp.API.Infrastructure
{
    public interface ITokenService
    {
        string CreateToken(AppUser user);
    }
}
