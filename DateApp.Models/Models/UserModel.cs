﻿namespace DateApp.Models.Models
{
    public class UserModel
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string PhotoUrl { get; set; }
    }
}
